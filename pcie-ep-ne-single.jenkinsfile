def AGENT = "pluto"

def configuration() {
	env.semifive_project		= "pcie-ep-ne-single"
    env.default_branch			= "master"
    env.test_name               = "pcie_ep_ne_single_sanity_test"

    env.notification_ms			= "true"
	env.office365ChannelName	= "devops5-development"
	env.office365Channel		= build(job: 'notification', parameters: [string(name: 'channel', value: "${env.office365ChannelName}")]).getBuildVariables()['FINAL']
}

def path_configuration(){
	env.rsyncPath = "/home/jenkins/Work/DevOps5/workspace/chronos_furiosa/success2_chronos_tapeout"
	env.archivePath = "/home/share/jenkins/archive"
	env.gitPath = "git@gitlab.semifive.com:FuriosaAI/${env.semifive_project}.git"
	env.fsdbPath = "/home/share/fsdb/${env.semifive_project}/${BUILD_NUMBER}"
	env.wavefilePath = "/home/share/furiosa_wavefile/${env.semifive_project}/${BUILD_NUMBER}"
}

def module_version(){
	env.ver_riscv = "riscv-tools/2019.08.0"
	env.ver_designware = "synopsys/designware/2019.12"
	env.ver_vcs = "synopsys/vcs/Q-2020.03"
	env.ver_verdi = "synopsys/verdi/Q-2020.03"
}


// do configuration
def do_configuration = configuration()
def do_path_configuration = path_configuration()
def do_module_version = module_version()

pipeline {
	/*
	*   agent
	*/
	agent {
		label "${AGENT}"
	}

    // options
	options {
        ansiColor('xterm')
        timestamps()
    }

	stages {

		/*
		*   stage('Prepare')
		*   Prepare before executing this build script.
		*/
		stage('Prepare'){
			steps {
				script{
                    // set environment for current build
					set_environment(env.default_branch)

					if(env.debug == "true"){
						println("[Debug] env.is_cleanBuild == ${env.is_cleanBuild}")
					}

					// build start notification via teams
					if (env.notification_ms == "true"){
						office365ConnectorSend(
							color: "#3deb34",
							message: "Build Start",
							factDefinitions : [[name : "Build Number", template : "${BUILD_NUMBER}"]],
							status: "start",
							webhookUrl: "${env.office365Channel}"
						)
					}
				}
			}
		}

		/*
			stage('Source')
			Clean current path and creat new workspace
		*/
		stage('Source') {
			steps {
				script{
                    // adjust workspace to configuration
					sh """ 
                        set +x
                        rm -rf build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/programs/*
						rsync -a ${env.rsyncPath}/ ./ 
					"""
                    try{
                        if (fileExists("${env.semifive_project}") == false){
                            sh """
                                git clone ${env.gitPath}
                                git -C ${env.semifive_project} checkout ${env.BRANCH}
                            """
                        }else {
                            sh"""
                                rm -rf ${env.semifive_project}
                                git clone ${env.gitPath}
                                git -C ${env.semifive_project} checkout ${env.BRANCH}
                            """
                        }
                    } catch(Error err){
                        sh "Nothing to Pull"
                    }
				}
			}
		}

		/*
			stage('Fast Build')
			compile C
		*/
		stage('Compile'){
			steps{
				script{
					try{
						sh """
							set +e
                            cd ${env.semifive_project}
							module load ${env.ver_riscv}
							make build
					    """	
					}catch(e){
						println("compile error")
						sh """
							set +x
							exit 1
						"""
					}						
				}
			}
			post{
			    success{
			        println("${STAGE_NAME} success")
			    }
			    
			    failure{
			        println("${STAGE_NAME} failure")
			    }
			    
			}
		}


		/*
			stage('Simulation')
			do simulation
		*/
		stage('Simulation'){
			steps {
				script{
					sh """
						module load ${env.ver_designware}
						module load ${env.ver_vcs}
						cd ${env.semifive_project}
						make simulation
					"""	
					check_especFailure(env.logPath)
				}
			}
			post{
			    success{
			        println("${STAGE_NAME} success")
			    }
			    
			    failure{
			        println("${STAGE_NAME} failure")
			    }
				aborted{
					println("${STAGE_NAME} abort")
				}
			}
		}
	}

	/*
		post stage
		after finishing all the above stages, regardless their status.
	*/
	post {
		/*
			always
			regardless the status
		*/
		always{
			script{
				try{
					archiveArtifacts artifacts: "${env.semifive_project}/sim.err", fingerprint:true
					archiveArtifacts artifacts: "${env.semifive_project}/sim.out", fingerprint:true					
					fsdb_extract(fsdbPath)
					dir("${env.fsdbPath}") {
						archiveArtifacts artifacts: "pcie-ep-single-sanity.fsdb", fingerprint:true
					}
				}catch(e){
					println("fsdb extract error")
					fsdb_recover(fsdbPath)
					dir("${env.fsdbPath}") {
						archiveArtifacts artifacts: "pcie-ep-single-sanity.fsdb", fingerprint:true
					}					
				}
				try{
					save_wavefile(wavefilePath)
				}catch(e){
					println("save wavefile error")
				}
				try{
					archive(archivePath)
				}catch(e){
					println("archive error")
				}				
			}          					
		}
		/*
			success
			when success
		*/
		success {

			updateGitlabCommitStatus(name : "jenkins", state : "success")
			script{
				if(env.notification_ms == "true"){
					office365ConnectorSend(
						color: "#3a10e3",
						message: "Build Success",
						factDefinitions : [[name : "Build Number", template : "${BUILD_NUMBER}"]],
						status: "Success",
						webhookUrl: "${env.office365Channel}"
					)
				}
			}
		}
		failure {

			updateGitlabCommitStatus(name : "jenkins", state : "failed")
			script{
				if(env.notification_ms == "true"){
					office365ConnectorSend(
						color: "#FA1304",
						message: "Build Failed",
						factDefinitions : [[name : "Build Number", template : "${BUILD_NUMBER}"]],
						status: "Failed",
						webhookUrl: "${env.office365Channel}"
					)
				}
			}
		}
		aborted {
			script{
				if(env.notification_ms == "true"){
					office365ConnectorSend(
						color: "#615357",
						message: "Build Aborted",
						factDefinitions : [[name : "Build Number", template : "${BUILD_NUMBER}"]],
						status: "Aborted",
						webhookUrl: "${env.office365Channel}"
					)
				}
			}
		}
	}
}

def fsdb_extract(fsdbPath){	

	lockList = []
	tmp_pwd  = sh(script: "pwd", returnStdout:true).trim()
	filePath = "${tmp_pwd}/${env.semifive_project}"

	maxTry   = 10

	if (fileExists("${env.fsdbPath}") == false){
		sh "mkdir -p ${env.fsdbPath}"
	}
	sh "cd ${filePath}"

	for (int i = 0; i < maxTry; i++){
		sh """ 
			set +x
			find . -name 'sim.fsdb.*' | tee check_fsdb_list.log > /dev/null 2>&1  
		"""
		file = readFile 'check_fsdb_list.log'
		list = file.split('\n')
  
		for(each in list){
			lockList.add(each)
		}

		if(lockList.isEmpty()){   
			sh """
				set +x
				cp ${env.semifive_project}/sim.fsdb ${env.fsdbPath}/
			"""   
		}else{
			sleep(30)
		}
	}//for i

	sh """ 
		set +x
		cp ${env.semifive_project}/sim.fsdb* ${env.fsdbPath}/
		cp /home/share/fsdb/fsdb_extract ${env.fsdbPath}/
	"""   
	sh """ 
		set +x
		cd ${env.fsdbPath}/
		module load ${env.ver_verdi} > /dev/null 2>&1  
		./fsdb_extract > /dev/null 2>&1  
	"""  
	dir("${env.fsdbPath}"){
		stash(name : "wavefiles", useDefaultExcludes: false, includes : "pcie-ep-single-sanity.fsdb" ) 
	}	
}

def fsdb_recover(fsdbPath){
    sh """ 
	  set +x
	  cp /home/share/fsdb/fsdb_recover_extract ${env.fsdbPath}/
      cd ${env.fsdbPath}/
      module load ${env.ver_verdi} > /dev/null 2>&1  
	  fsdbrecover sim.fsdb -o recover.fsdb > /dev/null 2>&1  
      ./fsdb_recover_extract > /dev/null 2>&1  
    """  
}

def archive(archivePath){

	if (fileExists("${env.archivePath}/${JOB_NAME}") == false){
		sh "mkdir -p ${env.archivePath}/${JOB_NAME}"
	}
	tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
	tmp_basedir = sh(script: "basename ${tmp_pwd}/${env.semifive_project}", returnStdout:true).trim()
	sleep(120)
	sh "tar -czf ${archivePath}/${JOB_NAME}/${JOB_NAME}.${BUILD_NUMBER}.tar.gz --exclude=.fuse ../${tmp_basedir}/${env.semifive_project}/"

	
}

def check_especFailure(logPath){
	tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
	dir("${tmp_pwd}/${env.semifive_project}") {
		read = readFile 'sim.err'
		if((read =~ /Assertion\s+`.*failed/)     //general assertion fail case
    || (read =~ /ExecutableSpec\#\d+\s+failed/))    //espec fail case
    {
			sh """
				set +x
				exit 1
			"""
		}
	}

}

def save_wavefile(wavefilePath){
	if (fileExists("${env.wavefilePath}") == false){
		sh "mkdir -p ${env.wavefilePath}"
	}	   
	node("master"){
		dir("${env.wavefilePath}"){
			unstash "wavefiles"
		}
	}
}

def set_environment(DEFAULT_BRANCH){

	if (env.wakeVersion == "0.17.2"){
		env.wakeRun	= "wake -v"
	} else {
		env.wakeRun = "wake -v -x"
	}
	tmp_cause = "unknown cause"
	tmp_username = "unknown username"
	tmp_branch = DEFAULT_BRANCH
	switch (currentBuild.getBuildCauses()._class[0]){
		case 'hudson.model.Cause$UserIdCause':
			tmp_cause =  "manual"
			tmp_username = currentBuild.getBuildCauses('hudson.model.Cause$UserIdCause').userName[0]
			tmp_branch = "master"
			break
		case 'com.dabsquared.gitlabjenkins.cause.GitLabWebHookCause':
			tmp_cause = "gitlab"
			tmp_username = "$gitLabUserName"
			tmp_branch = "$gitlabAfter"
			break
		case 'hudson.triggers.TimerTrigger$TimerTriggerCause':
			tmp_cause =  "periodically"
			tmp_username = "periodically"
			tmp_branch = "master"
			break
		case 'org.jenkinsci.plugins.gwt.GenericCause':
			tmp_cause = "generic webhook (to be updated soon)"
			tmp_username = "generic webhook (to be updated soon)"
			break
		case 'hudson.model.Cause$UpstreamCause':
			tmp_cause = "upstream"
			tmp_username = "upstream"
			tmp_branch = "master"

		default:
			break
	}
	if (tmp_cause != "gitlab"){
		if(tmp_cause == "upstream"){
				currentBuild.setDescription("Build from ${tmp_cause} from ${currentBuild.getBuildCauses().upstreamProject} at ${currentBuild.getBuildCauses().upstreamBuild}")
		} else if(tmp_cause != "periodically"){
			currentBuild.setDescription("Build from ${tmp_username} with ${tmp_cause}")
		} else {
			currentBuild.setDescription("Build ${tmp_cause}")
		}
	}

	x = currentBuild.getDescription()
	currentBuild.setDescription("Node on ${NODE_NAME}\n${x}")

	env.CAUSE = tmp_cause
	env.USER = tmp_username
	env.BRANCH = tmp_branch
}
